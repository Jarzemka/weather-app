import React from "react";
import { HomeContent } from "../../contents";

const Home = () => {
  return (
      <HomeContent />
  );
};

export default Home;
