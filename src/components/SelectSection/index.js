import React from "react";
import injectSheet from "react-jss";
import styles from "./styles";
import { Input, Button } from "../";
import { useTranslation } from "react-i18next";

const SelectSection = ({
  classes,
  handleOnChange,
  handleFetchWeatherData,
  handleFetchDataByCor
}) => {

  const { t } = useTranslation();

  return (
    <>
      <h1 className={classes.PageTitle}>{t("title")}</h1>
      <div className={classes.Section}>
        <Input
          inputPlaceholder={t("input.location")}
          handleOnChange={handleOnChange}
        />
        <div className={classes.ButtonSection}>
          <Button
            buttonTitle={t("button.select")}
            handleButtonOnClick={handleFetchWeatherData}
          />
          <div className={classes.Or}>{t("or")} </div>
          <Button
            buttonTitle={t("button.findYourLocation")}
            handleButtonOnClick={handleFetchDataByCor}
          />
        </div>
      </div>
    </>
  );
};

export default injectSheet(styles)(SelectSection);
