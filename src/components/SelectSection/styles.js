const styles = theme => ({
  PageTitle: {
    color: theme.colors.red.primary,
    fontWeight: "bold",
    width: "100%",
    textAlign: "center",
    "@media (min-width: 768px)": {}
  },
  Section: {
    backgroundColor: "white",
    height: "140px",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "space-between",
    border: `2px solid ${theme.colors.red.primary}`,
    padding: "30px",
    margin: "30px",
    borderRadius: "5px",
    "@media (min-width: 768px)": {}
  },
  ButtonSection: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  Or: {
    margin: '5px 0'
  }
});

export default styles;
