const styles = theme => ({
  Footer: {
    // position: "absolute",
    // bottom: "0",
    // left: "0",
    // width: "100%"
  },
  FooterContainer: {
    backgroundColor: "#331832",
    color: theme.colors.font.foot,
  },
  FooterCol: {
    height: "100px"
  },

  FooterContent: {
    padding: "20px 10px"
  }
});

export default styles;
