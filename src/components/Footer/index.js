import React from "react";
import { Container, Row, Col } from "react-grid-system";
import injectSheet from "react-jss";
import styles from "./styles";
import { useTranslation } from "react-i18next";

const Footer = ({ classes }) => {
  const { t } = useTranslation();
  return (
    <div className={classes.Footer}>
      <Container fluid className={classes.FooterContainer}>
        <Row>
          <Col md={12} className={classes.FooterCol}>
            <Row>
              <Col className={classes.FooterContent}>
                <div>{t("footer.author")}: Małgorzata Jarzemska</div>
                <div>{t("footer.weatherApp")}</div>
                <div>02.2020</div>
              </Col>
            </Row>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default injectSheet(styles)(Footer);
