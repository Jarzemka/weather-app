const styles = theme => ({
  Input: {
    width: "200px",
    padding: "5px 10px",
    fontSize: "16px",
    borderRadius: "5px",
    textAlign: "center"
  }
});

export default styles;
