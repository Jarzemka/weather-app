import React from "react";
import injectSheet from "react-jss";
import styles from "./styles";

const Input = ({
  inputType,
  inputValue,
  inputId,
  handleOnChange,
  classes,
  inputPlaceholder
}) => {
  return (
    <input
      type={inputType}
      value={inputValue}
      id={inputId}
      onChange={handleOnChange}
      placeholder={inputPlaceholder}
      className={classes.Input}
    />
  );
};

export default injectSheet(styles)(Input);
