import React, { useState } from "react";
import { Container, Row, Col, Visible } from "react-grid-system";
import NavItems from "./NavItems/index";
import MobileNavIcon from "./MobileNavIcon/index";
import injectSheet from "react-jss";
import styles from "./styles";

const Navigation = ({ classes }) => {
  const [navIsOpen, setNavIsOpen] = useState(false);
  const handleMobileNavigation = () => {
    setNavIsOpen(!navIsOpen);
  };
  return (
    <div className={classes.Navigation}>
      <Container fluid className={classes.NavContainer}>
        <Row className={classes.NavRow}>
          <Col className={classes.NavCol} md={12}>
            <Row className={classes.NavRow}>
              <Col className={classes.NavCol} xl={10} offset={{ xl: 1 }}>
                <Visible xs sm>
                  <MobileNavIcon
                    navIsOpen={navIsOpen}
                    clickedIcon={handleMobileNavigation}
                  />
                  <div
                    className={
                      navIsOpen ? classes.NavOpened : classes.NavClosed
                    }
                  >
                    <NavItems />
                  </div>
                </Visible>
                <Visible md lg xl>
                  <div className={classes.NavItems}>
                    <NavItems />
                  </div>
                </Visible>
              </Col>
            </Row>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default injectSheet(styles)(Navigation);
