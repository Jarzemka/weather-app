import React from "react";
import injectSheet from "react-jss";
import styles from "./styles";

const MobileNavIcon = ({ navIsOpen, clickedIcon, classes }) => {
  return (
    <div className={classes.MobileNav}>
      <div className={classes.HamburgerWrapper} onClick={clickedIcon}>
        {navIsOpen ? (
          <div className={classes.CloseIcon} onClick={clickedIcon} />
        ) : (
          <>
            <div className={classes.Hamburger} />
            <div className={classes.Hamburger} />
            <div className={classes.Hamburger} />
          </>
        )}
      </div>
    </div>
  );
};

export default injectSheet(styles)(MobileNavIcon);
