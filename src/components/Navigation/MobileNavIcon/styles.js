const styles = theme => ({
  MobileNav: {
    height: "100%",
    width: "100%",
    display: "flex",
    alignItems: "center",
  },
  HamburgerWrapper: {
    width: "25px",
    height: "25px",
    margin: "0px 15px",
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-around",
    padding: "2px",
    borderRadius: "4px",
    boxSizing: "border-box"
  },
  Hamburger: {
    width: "100%",
    height: "1.5px",
    borderRadius: "2px",
    backgroundColor: "#f9dec9"
  },
  CloseIcon: {
    width: "100%",
    height: "100%",
    position: "relative",
    "&::before, &::after": {
      position: "absolute",
      content: "''",
      height: "100%",
      width: "2px",
      backgroundColor: "#f9dec9",
      top: 0
    },
    "&::before": {
      left: "9px",
      transform: "rotate(45deg)"
    },
    "&::after": {
      right: "10px",
      transform: "rotate(-45deg)"
    }
  }
});

export default styles;
