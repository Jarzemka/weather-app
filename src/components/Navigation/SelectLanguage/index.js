import React, { useState } from "react";
import injectSheet from "react-jss";
import styles from "./styles";
import { useTranslation } from "react-i18next";

const SelectLanguage = ({ classes }) => {
  const { i18n } = useTranslation();
  const [selectedLang, setSelectedLang] = useState(i18n.language);
  const handleSelectLanguage = lang => {
    i18n.changeLanguage(lang);
    setSelectedLang(lang);
  };
  return (
    <div className={classes.Wrapper}>
      <div
        onClick={() => handleSelectLanguage("pl")}
        className={`${classes.Option} ${selectedLang === "pl" &&
          classes.SelectedLanguage}`}
      >
        PL
      </div>
      <div
        onClick={() => handleSelectLanguage("en")}
        className={`${classes.Option} ${selectedLang === "en" &&
          classes.SelectedLanguage}`}
      >
        EN
      </div>
    </div>
  );
};

export default injectSheet(styles)(SelectLanguage);
