const styles = theme => ({
  Wrapper: {
    display: "flex",
    flexDirection: "row",
    width: "50px",
    justifyContent: "space-between",
    color: theme.colors.orange.primary
  },
  Option: {
    opacity: "0.5",
    "&:hover": {
      cursor: "pointer"
    }
  },
  SelectedLanguage: {
    borderBottom: `2px solid ${theme.colors.orange.primary}`,
    opacity: "1"
  }
});

export default styles;
