const styles = theme => ({
  Navigation: {
    width: "100vw",
  },
  NavContainer: {
    paddingLeft: "unset !important",
    paddingRight: "unset !important",
    backgroundColor: theme.colors.red.primary,
    boxShadow: `5px 10px 18px ${theme.colors.gray.shadow}`
  },
  NavRow: {
    marginLeft: "unset !important",
    marginRight: "unset !important",
    height: "50px"
  },
  NavCol: {
    paddingRight: "unset !important",
    paddingLeft: "unset !important",
    height: "100%"
  },
  NavItems: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "flex-end",
    alignItems: "center",
    boxSizing: "border-box",
    height: "100%",
    width: "100%"
  },
  NavClosed: {
    height: "0",
    opacity: "0",
    transition: "all 0.3s ease-in-out"
  },
  NavOpened: {
    height: "100vh",
    width: "100vw",
    backgroundColor: "#773344",
    opacity: "1",
    transition: "height 0.5s ease-in-out",
    zIndex: "200"
  }
});

export default styles;
