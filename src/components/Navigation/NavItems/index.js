import React from "react";
import { ReactLink } from "../../index";
import SelectLanguage from "../SelectLanguage/index";
import { links } from "../../../constants";
import injectSheet from "react-jss";
import styles from "./styles";

const NavItems = ({ classes }) => {
  return (
    <div className={classes.MobileNavContent}>
      <SelectLanguage />
    </div>
  );
};

export default injectSheet(styles)(NavItems);
