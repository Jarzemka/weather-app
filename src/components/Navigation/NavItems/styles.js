const styles = theme => ({
  MobileNavContent: {
    height: "80%",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "space-around",
    "@media (min-width: 768px)": {
      flexDirection: "row"
    }
  },
  NavItem: {
    margin: "0 10px",
    color: `${theme.colors.font.nav} !important`,
    fontSize: "20px",
    "@media (min-width: 768px)": {
      margin: "0 10px",
      display: "flex",
      alignItems: "center",
      fontSize: "16px"
    },
    "&:hover": {
      borderBottom: `2px solid ${theme.colors.orange.primary} !important`
    }
  }
});

export default styles;
