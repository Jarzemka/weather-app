import React from "react";
import { Link } from "react-router-dom";
import injectSheet from "react-jss";
import styles from "./styles";

const ReactLink = ({ path, title, className, classes }) => {
  return (
    <Link className={`${classes.ReactLink} ${className}`} to={path}>
      {title}
    </Link>
  );
};

export default injectSheet(styles)(ReactLink);
