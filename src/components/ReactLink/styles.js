const styles = theme => ({
  ReactLink: {
    textDecoration: "none",
    color: "black",
    "&:hover": {
      cursor: "pointer",
      borderBottom: "1px solid black"
    }
  }
});

export default styles;
