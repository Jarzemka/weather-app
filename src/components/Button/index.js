import React from "react";
import injectsheet from "react-jss";
import styles from "./styles";

const Button = ({ buttonTitle, handleButtonOnClick, classes }) => (
  <button className={classes.Button} onClick={handleButtonOnClick}>
    {buttonTitle}
  </button>
);

export default injectsheet(styles)(Button);
