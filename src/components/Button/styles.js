const styles = theme => ({
  Button: {
    width: "220px",
    padding: "8px 10px",
    fontSize: "14px",
    borderRadius: "5px",
    backgroundColor: theme.colors.red.primary,
    color: 'white',
    border: 'unset',
    "&:hover": {
      cursor: "pointer"
    },
    "&:focus": {
      outline: "none"
    }
  }
});

export default styles;
