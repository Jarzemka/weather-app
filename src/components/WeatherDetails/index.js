import React from "react";
import styles from "./styles";
import injectSheet from "react-jss";
import { Button } from "../";
import { useTranslation } from "react-i18next";

const WeatherDetails = ({ description, city, classes, handleReset }) => {
  const { t } = useTranslation();
  return (
    <div className={classes.SectionWrapper}>
      <h1 className={classes.Title}>
        {t("weatherFor")} <span className={classes.CityName}> {city} </span>
      </h1>
      <div className={classes.Details}>
        {description.weather.map((item, index) => (
          <div key={index}>
            <img
              alt="weatherIcon"
              src={`http://openweathermap.org/img/wn/${item.icon}@2x.png`}
              className={classes.WeatherIcon}
            />
            <div className={classes.WeatherDescription} key={index}>
              {item.description}
            </div>
          </div>
        ))}
        <div className={classes.DetailsWrapper}>
          <div className={classes.Key}>
            {t("weatherDescription.temperature")}:
            <span className={classes.Value}>
              {Math.floor(description.main.temp)}&deg;C
            </span>
          </div>
          <div className={classes.Key}>
            {t("weatherDescription.feelsLike")}:
            <span className={classes.Value}>
              {Math.floor(description.main.feels_like)}
              &deg;C
            </span>
          </div>
          <div className={classes.Key}>
            {t("weatherDescription.windSpeed")}:
            <span className={classes.Value}>{description.wind.speed}km/h </span>
          </div>
          <div className={classes.Key}>
            {t("weatherDescription.pressure")}:
            <span className={classes.Value}>
              {description.main.pressure}hPa
            </span>
          </div>
        </div>
      </div>
      <Button
        buttonTitle={t("button.back")}
        handleButtonOnClick={handleReset}
      />
    </div>
  );
};

export default injectSheet(styles)(WeatherDetails);
