const styles = theme => ({
  SectionWrapper: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  Title: {
    width: "100%",
    textAlign: "center",
    margin: "unset"
  },
  CityName: {
    color: theme.colors.red.primary
  },
  WeatherIcon: {
    width: "150px",
    height: "150px",
    display: "flex",
    margin: "0 auto"
  },
  WeatherDescription: {
    fontWeight: "bold",
    textAlign: "center",
    marginBottom: "10px"
  },
  Details: {
    margin: "15px auto",
    width: "200px",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    "@media (min-width: 768px)": {
      margin: "0px auto 8px auto"
    },
    "@media (min-width: 1366px)": {
      margin: "30px auto"
    }
  },
  DetailsWrapper: {
    width: "100%"
  },
  Key: {
    marginTop: "5px",
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between"
  },
  Value: {
    color: theme.colors.red.primary
  }
});

export default styles;
