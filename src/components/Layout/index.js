import React from "react";
import { Navigation, Footer } from "../../components";
import injectSheet from "react-jss";
import styles from "./styles";

const Layout = ({ children, classes }) => {
  return (
    <div className={classes.Layout}>
      <Navigation />
      {children}
      <Footer />
    </div>
  );
};

export default injectSheet(styles)(Layout);
