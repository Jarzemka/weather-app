const links = {
  path: {
    home: "/",
    today: "/today",
    tommorow: "/tommorow",
    longPeriot: "/longPeriot"
  }
};

export default links;
