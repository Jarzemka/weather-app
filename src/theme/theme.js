const theme = {
  colors: {
    red: {
      primary: "#773344"
    },
    gray: {
      shadow: "#888888"
    },
    orange: {
      primary: "#f9dec9"
    },
    font: {
      nav: "#f9dec9",
      foot: "#D7DEDC"
    },
    background: {
      home: "#D7DEDC"
    }
  },
  mq: {
    mobile: `@media (min-width: 576px)`,
    tablet: `@media (min-width: 768px)`,
    laptop: `@media (min-width: 1366px)`,
    desktop: `@media (min-width: 1920px)`
  }
};

export default theme;
