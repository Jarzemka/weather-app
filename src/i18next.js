import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import LanguageDetector from "i18next-browser-languagedetector";
import en from "./locales/en";
import pl from "./locales/pl";

i18n
  .use(LanguageDetector)
  .use(initReactI18next)
  .init({
    ns: ["common"],
    defaultNS: "common",
    fallbackLng: "en",
    debug: true,
    resources: {
      en,
      pl
    },
    interpolation: {
      escapeValue: false
    },
    react: {
      wait: true
    }
  });

export default i18n;
