import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import { ThemeProvider } from "react-jss";
import theme from "./theme/theme";
import { Provider } from "react-redux";
import { createStore, combineReducers, applyMiddleware } from "redux";
import { WeatherReducer } from "./store";
import logger from "redux-logger";
import promiseMiddleware from "redux-promise-middleware";
import thunk from "redux-thunk";

const rootReducers = combineReducers({
  details: WeatherReducer
});

const store = createStore(
  rootReducers,
  applyMiddleware(promiseMiddleware, thunk, logger)
);

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <Provider store={store}>
      <App />
    </Provider>
  </ThemeProvider>,
  document.getElementById("root")
);

serviceWorker.unregister();
