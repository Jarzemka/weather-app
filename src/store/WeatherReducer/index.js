import {
  GET_WEATHER_INFO,
  GET_CITY_NAME,
  GET_WEATHER_INFO_BY_COOR
} from "../types";

const initialState = {
  info: [],
  status: "",
  cityName: ""
};

const WeatherReducer = (state = initialState, action) => {
  switch (action.type) {
    case `${GET_WEATHER_INFO}_PENDING`: {
      return {
        ...state,
        status: "PENDING",
        data: null
      };
    }
    case `${GET_WEATHER_INFO}_REJECTED`: {
      return {
        ...state,
        status: "REJECTED",
        data: null
      };
    }
    case `${GET_WEATHER_INFO}_FULFILLED`: {
      return {
        ...state,
        status: "FULFILLED",
        info: action.payload
      };
    }
    case `${GET_WEATHER_INFO_BY_COOR}_PENDING`: {
      return {
        ...state,
        status: "PENDING",
        data: null
      };
    }
    case `${GET_WEATHER_INFO_BY_COOR}_REJECTED`: {
      return {
        ...state,
        status: "REJECTED",
        data: null
      };
    }
    case `${GET_WEATHER_INFO_BY_COOR}_FULFILLED`: {
      return {
        ...state,
        status: "FULFILLED",
        info: action.payload
      };
    }
    case `${GET_CITY_NAME}`: {
      return {
        ...state,
        cityName: action.payload
      };
    }
    case `RESET_REDUCER`: {
      return {
        ...initialState
      };
    }
    default:
      return state;
  }
};

export default WeatherReducer;
