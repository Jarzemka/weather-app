import {
  GET_WEATHER_INFO,
  GET_CITY_NAME,
  RESET_REDUCER,
  GET_WEATHER_INFO_BY_COOR
} from "../types";
import { keys } from "../../constants";
import axios from "axios";

export const getCurrentWeatherInfo = (city, lang) => {
  const url = `http://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${keys.weatherApiKey}&lang=${lang}&units=metric`;
  const request = axios.get(url);
  return {
    type: GET_WEATHER_INFO,
    payload: request
  };
};
export const getCurrentWeatherInfoByCoor = (lat, lon, lang) => {
  const url = `https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&appid=${keys.weatherApiKey}&lang=${lang}&units=metric`;
  const request = axios.get(url);
  return {
    type: GET_WEATHER_INFO_BY_COOR,
    payload: request
  };
};

export const getCityName = cityName => {
  return {
    type: GET_CITY_NAME,
    payload: cityName
  };
};

export const resetReducer = () => {
  return {
    type: RESET_REDUCER
  };
};
