const styles = theme => ({
  PageWrapper: {
    width: "100%",
    height: "calc(100% - 150px)",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column"
  }
});

export default styles;
