import React, { useState } from "react";
import injectSheet from "react-jss";
import { WeatherDetails, SelectSection } from "../../components";
import styles from "./styles";
import { connect } from "react-redux";
import {
  getCurrentWeatherInfo,
  getCityName,
  resetReducer,
  getCurrentWeatherInfoByCoor
} from "../../store/WeatherReducer/action";
import { get } from "lodash";
import { useTranslation } from "react-i18next";

const HomeContent = ({
  classes,
  onGetWeatherInfo,
  onGetWeatherInfoByCoor,
  onGetCityName,
  onResetReducer,
  location,
  weatherDescription,
  status
}) => {
  const [cityName, setCity] = useState(location);
  const { t, i18n } = useTranslation();
  const geo = navigator.geolocation;
  const handleFetchWeatherData = city => {
    if (!city) {
      return alert("nie podano miasta");
    }
    onGetCityName(city);
    onGetWeatherInfo(city, i18n.language);
  };
  const handleFetchDataByCor = () => {
    geo.getCurrentPosition(loc => {
      let lati = loc.coords.latitude;
      let lon = loc.coords.longitude;
      onGetWeatherInfoByCoor(lati.toFixed(2), lon.toFixed(2), i18n.language);
    });
  };
  if (status === "PENDING") {
    return <div className={classes.PageWrapper}>{t("statuses.pending")}</div>;
  }
  if (status === "REJECTED") {
    return <div className={classes.PageWrapper}>{t("statuses.error")}</div>;
  }

  return (
    <div className={classes.PageWrapper}>
      {weatherDescription ? (
        <WeatherDetails
          description={weatherDescription}
          city={location ? location : weatherDescription.name}
          handleReset={onResetReducer}
        />
      ) : (
        <SelectSection
          handleOnChange={event => setCity(event.target.value)}
          handleFetchWeatherData={() => handleFetchWeatherData(cityName)}
          handleFetchDataByCor={handleFetchDataByCor}
        />
      )}
    </div>
  );
};

const mapStateToProps = state => ({
  location: state.details.cityName,
  weatherDescription: get(state.details, "info.data"),
  status: state.details.status
});

const mapDispatchToProps = {
  onGetWeatherInfo: getCurrentWeatherInfo,
  onGetCityName: getCityName,
  onResetReducer: resetReducer,
  onGetWeatherInfoByCoor: getCurrentWeatherInfoByCoor
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(injectSheet(styles)(HomeContent));
