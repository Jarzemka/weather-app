module.exports = {
  module: {
    rules: [
      {
        test: /\.css$/i,
        loader: "css-loader",
        options: {
          import: true
        }
      },
      {
        "at-rule-no-unknown": [
          true,
          {
            ignoreAtRules: [
              "function",
              "if",
              "each",
              "include",
              "mixin",
              "value"
            ]
          }
        ]
      }
    ]
  }
};
